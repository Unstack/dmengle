import sys
import json

# Microutility for parsing JSON data from googler to dmenu format
# Expects "in" or "out" as first parameter, defining the primary action:
# in:
# 	- Expects Googler JSON data as second to last parameter
#	- JSON data will be congegrated and parsed into google information
#	- Writes google information to /tmp/dmengle.cache
# out (default):
#	- Expects an abstract as second parameter ( selected through dmenu)
#	- Matches the abstract to the corresponding URL in /tmp/dmeng;e.cache
#	- Opens corresponding URL in default browser


def pretty(data):
    try:
        searchdata = json.loads(data)

        for entry in searchdata:
            print(entry['abstract'])

    except json.JSONDecodeError as err:
        print("Err 2: Parsing request JSON failed.")

    open("/tmp/dmengle.cache", 'w').write(data)

    print("[ Back ]")
    print("[ Cancel ]")


def select(data):
    try:
        searchdata = json.loads(open("/tmp/dmengle.cache", 'r').read())
        for entry in searchdata:
            if data in entry['abstract']:
                print(entry['url'])
                break

    except FileNotFoundError:
        print("Err 3: Cache not found or invalid")


if __name__ == "__main__":
    if len(sys.argv) <= 1:
        print("Err 0: Not enough arguments")

    if sys.argv[1] == 'in':
        pretty(" ".join(sys.argv[2:]))
    elif sys.argv[1] == 'out':
        select(" ".join(sys.argv[2:]))
    else:
        print("Err 1: Invalid arguments (first parameter must be 'in' or 'out)")
