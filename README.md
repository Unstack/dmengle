# dmengle 

Minimalist dmenu hack for querying Google, based on Googler.

![Dmengle demonstration](https://i.imgur.com/AIcbb17.gif)

## Todo
- Paging
- Cache results by query
- List cached entries as previous searches
- Load last cached result on empty search
- Drop googler dependency for curl

## Requirements

-   Python 2.7 / 3.0
-   [Googler](https://github.com/jarun/googler)

## Installation Guide

Just extract both files to your ~/bin folder to have it work instantly.

Or any other bin folder of your liking really, just adjust the config
on top of the dmengle script to point towards wherever you have the python 
script installed.

## Configuration

Set DMENGLE_FAST in your environment to skip the validation menu.

You can adjust the amount of results, search country, font size and fastsearch
from the top of the dmengle script.


## Usage

Bind 'dmengle' to your favourite keyboard key. Enjoy.

## Credits

-   [Jarun](https://github.com/jarun) - [Googler](https://github.com/jarun/googler)

